# gsearch-image

Run a google image search and download thumbnails. Script filters for photos only (not drawings / clip art / etc). Images will be downloaded into a /images/<query_term> folder. This script was put together to easily get images for learning about image classification using neural networks.

## Uses
*  Python 3.6
*  Docker

## Setup
### .env_file
1.  Enter your [google api key](https://console.cloud.google.com/)
2.  Create a custom search engine using the Custom Search [Control Panel](https://cse.google.com/cse/all):
3.  Enter your cx id that can be found under the **Basics** tab and **Details** section

### params used
1.  query_term - search keyword(s)
2.  total_cycles - number of times to query google api for 10 records (ie: total_cycles=2 will get 20 images / total_cycles=3 will get 30 images)

### build container
*  docker build -t py/gsearch .

### run script to search and download images
*  **cmd:** docker run -it --rm -v "$PWD":/usr/src/app --env-file .env_file py/gsearch python3 scripts/gsearch.py 'query_term' times_to_run_search
*  **example:** docker run -it --rm -v "$PWD":/usr/src/app --env-file .env_file py/gsearch python3 scripts/gsearch.py 'harry potter' 2
