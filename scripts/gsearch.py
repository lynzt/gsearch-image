#!/usr/bin/python
import requests
import shutil
import json
import sys
import os

# use google image search and download image thumbnails
# params:
#   query_term - search keyword(s)
#   total_cycles - number of times to hit google api for 10 records (ie: total_cycles=2 will get 20 images / total_cycles=3 will get 30 images)
#

def download_image(image_url, filename):
    response = requests.get(image_url, stream=True)
    with open(filename, 'wb') as out_file:
        shutil.copyfileobj(response.raw, out_file)
    del response

def process_data(data, path, filename, image_start_nbr):
    for d in data['items']:
        print ('download: {}'.format(d['title']))
        image_filename = '{}/{}.{}.jpg'.format(path, filename, image_start_nbr)
        download_image(d['image']['thumbnailLink'], image_filename)
        image_start_nbr+=1

def hit_url(query_term, start_index):
    url = 'https://www.googleapis.com/customsearch/v1'
    payload = {
        'key': os.environ['API_KEY'],
        'cx': os.environ['CX'],
        'searchType': 'image',
        'imgType': 'photo',
        'q': query_term
    }
    if start_index:
        payload['start'] = start_index
    print (payload)
    r = requests.get(url, params=payload)
    return r.json()

def create_dirs(dirs):
    if not os.path.exists(dirs):
        os.makedirs(dirs)

def pretty_print(json_data):
    print (json.dumps(json_data, indent=2, sort_keys=True))

def main(query_term, total_cycles):
    filename_base = query_term.replace(" ", "_")
    path = 'images/{}'.format(filename_base)
    create_dirs(path)

    for i in range(int(total_cycles)):
        image_start_nbr = 10 * i
        start_index = None if i == 0 else search_results['queries']['nextPage'][0]['startIndex']
        search_results = hit_url(query_term, start_index)
        process_data(search_results, path, filename_base, image_start_nbr)

if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])
